package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.Branch;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(BranchService.NAME)
public class BranchServiceBean implements BranchService {

    @Inject
    private EntityService<Branch> entityService;

    /**
     * Возвращает список отделений для выбранных года, кафедры и факультета.
     * @param year Год.
     * @param chairId ID кафедры.
     * @param facultyId ID факультета.
     * @return Список отделений.
     */
    public List<Branch> getBranches(Object year, Object chairId, Object facultyId) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, year);
        arguments.put(2, chairId);
        arguments.put(3, facultyId);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_branch(?, ?, ?)", Branch.class, arguments);
    }
}