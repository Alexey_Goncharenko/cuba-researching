package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.Chair;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(ChairService.NAME)
public class ChairServiceBean implements ChairService {

    @Inject
    private EntityService<Chair> entityService;

    /**
     * Возвращает список кафедр.
     * @param year Год.
     * @return Список кафедр, упорядоченных по алфавиту.
     */
    public List<Chair> getChairs(Object year) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, year);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_chair(?)", Chair.class, arguments);
    }
}