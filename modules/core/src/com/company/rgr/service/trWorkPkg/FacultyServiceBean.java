package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.Faculty;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(FacultyService.NAME)
public class FacultyServiceBean implements FacultyService {

    @Inject
    private EntityService<Faculty> entityService;

    /**
     * Возвращает список факультетов для выбранных года и кафедры.
     * @param year Год.
     * @param chairId ID кафедры.
     * @return Список факультетов.
     */
    public List<Faculty> getFaculties(Object year, Object chairId) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, year);
        arguments.put(2, chairId);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_facultet(?, ?)", Faculty.class, arguments);
    }
}