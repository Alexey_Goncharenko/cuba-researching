package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.LinkedPerson;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(LinkedPersonService.NAME)
public class LinkedPersonServiceBean implements LinkedPersonService {

    @Inject
    private EntityService<LinkedPerson> entityService;

    /**
     * Возвращает список преподавателей на указанной кафедре в указанном году.
     * @param year Год.
     * @param chairId ID кафедры.
     * @return Список преподавателей.
     */
    public List<LinkedPerson> getLinkedPersons(Object year, Object chairId) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, year);
        arguments.put(2, chairId);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_linked_persons(?, ?)", LinkedPerson.class, arguments);
    }

}