package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.DistributionLine;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(DistributionLineService.NAME)
public class DistributionLineServiceBean implements DistributionLineService {

    @Inject
    private EntityService<DistributionLine> entityService;

    /**
     * Возвращает распределение нагрузи.
     * @param pk PK.
     * @param type Type.
     * @return Распределение нагрузки.
     */
    public List<DistributionLine> getDistributionLines(Object pk, Object type) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, pk);
        arguments.put(2, type);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_distr_lines(?, ?)", DistributionLine.class, arguments, true);
    }

    /**
     * Изменяет запись DistributionLine в соответствии с параметрами.
     */
    public void editDistributionLine(
            Object fkPersonLine,
            Object pk,
            Object type,
            Object fkTrWorkChair,
            Object personId,
            Object weeksCountL,
            Object studentsCountL,
            Object fkTypeStudyWork,
            Object fkGroup,
            Object sgNum,
            Object hours,
            Object currentHours,
            Object lecturerHours
    ) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, fkPersonLine);
        arguments.put(2, pk);
        arguments.put(3, type);
        arguments.put(4, fkTrWorkChair);
        arguments.put(5, personId);
        arguments.put(6, weeksCountL);
        arguments.put(7, studentsCountL);
        arguments.put(8, fkTypeStudyWork);
        arguments.put(9, fkGroup);
        arguments.put(10, sgNum);
        arguments.put(11, hours);
        arguments.put(12, currentHours);
        arguments.put(13, lecturerHours);

        entityService.execute("decanatuser.tr_work_pkg.set_line2(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", arguments);
    }

}