package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.Person;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(PersonService.NAME)
public class PersonServiceBean implements PersonService {

    @Inject
    private EntityService<Person> entityService;

    /**
     * Возвращает список преподавателей на указанной кафедре.
     * @param chairId ID кафедры.
     * @param checkboxValue 1, если только с кафедры; 0 иначе.
     * @return Список преподавателей.
     */
    public List<Person> getPersons(Object chairId, Object checkboxValue) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, chairId);
        arguments.put(2, checkboxValue);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_persons(?, ?)", Person.class, arguments);
    }

}