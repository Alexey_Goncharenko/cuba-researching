package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.SrcLine;
import com.company.rgr.service.EntityService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(SrcLineService.NAME)
public class SrcLineServiceBean implements SrcLineService {

    @Inject
    private EntityService<SrcLine> entityService;

    /**
     * Возвращает нагрузку в указанном году я выбранных кафедры, отделения и преподавателя.
     * @param year Год.
     * @param chairId ID кафедры.
     * @param branchId ID отделения.
     * @param linkedPersonId ID преподавателя.
     * @return Нагрузка.
     */
    public List<SrcLine> getSrcLines(Object year, Object chairId, Object branchId, Object linkedPersonId) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, year);
        arguments.put(2, chairId);
        arguments.put(3, branchId);
        arguments.put(4, linkedPersonId);

        return entityService.fetchData("decanatuser.tr_work_pkg.get_src_lines(?, ?, ?, ?)", SrcLine.class, arguments, true);
    }

}