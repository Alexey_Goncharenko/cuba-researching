package com.company.rgr.service;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import oracle.jdbc.OracleTypes;
import org.eclipse.persistence.exceptions.JPQLException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

@Service(CommonService.NAME)
public class CommonServiceBean implements CommonService {

    @Inject
    private Persistence persistence;

    /**
     * Возвращает наиболее вероятно используемый в данный момент год.
     * @return Год.
     */
    public int getUsefulYear() {
        Transaction tx = persistence.createTransaction();
        int result = 0;
        try {
            EntityManager em = persistence.getEntityManager();
            Connection conn = em.getConnection();

            CallableStatement stmt = conn.prepareCall("{ ? = call DECANATUSER.TR_WORK_PKG.GET_YEAR() }");
            stmt.registerOutParameter(1, OracleTypes.INTEGER);

            stmt.execute();
            result = stmt.getInt(1);

            tx.commit();
        }
        catch (JPQLException | SQLException e) {
            e.printStackTrace();
        } finally {
            tx.end();
        }

        return result;
    }

    /**
     * Аутентифицирует пользователя в БД.
     * @param login Логин.
     * @param password Пароль.
     * @return Возвращает id пользователя.
     */
    public int authenticate(String login, String password) {
        Transaction tx = persistence.createTransaction();
        int userId = 0;
        try {
            EntityManager em = persistence.getEntityManager();
            Connection conn = em.getConnection();

            CallableStatement stmt = conn.prepareCall("{ call CMOP.AUTH_PKG.CONNECT_USER(?, ?, ?, ?, ?) }");
            stmt.setString(1, login);
            stmt.setString(2, password);
            stmt.registerOutParameter(3, OracleTypes.VARCHAR);
            stmt.registerOutParameter(4, OracleTypes.NUMBER);
            stmt.setInt(5, 0);

            stmt.execute();

            userId = stmt.getInt(4);

            tx.commit();
        }
        catch (JPQLException | SQLException e) {
            e.printStackTrace();
        } finally {
            tx.end();
        }

        return userId;
    }

}