package com.company.rgr.service;

import com.company.rgr.service.helpers.ResultSetMapper;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.app.UniqueNumbersAPI;
import oracle.jdbc.OracleTypes;
import org.eclipse.persistence.exceptions.JPQLException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(EntityService.NAME)
public class EntityServiceBean<T> implements EntityService<T> {

    @Inject
    private UniqueNumbersAPI uniqueNumbers;

    @Inject
    private Persistence persistence;

    /**
     * Забирает список из сущностей, возвращаемых хранимой процедурой через ref_cursor.
     * @param storedFunctionWithArguments Строка с вопросами вместо аргументов. Пример: "decanatuser.tr_work_pkg.get_facultet(?, ?)"
     * @param outputClass Класс объекта, элементы которого будут составлять список. Должен иметь аннтоацию @Entity, а поля должны иметь аннотации @Column(name = "COLNAME")
     * @param arguments Словарь из аргументов. Ключ -- номер аргумента (начинается с 1), значение -- подставляемое на его место значение.
     * @param generateIds True, если процедура не возвращает уникального ключа, и его необходимо генерировать.
     * @return Возвращает пустой или непустой List, всегда не null.
     */
    public List<T> fetchData(String storedFunctionWithArguments, Class outputClass, HashMap<Integer, Object> arguments, boolean generateIds) {
        ResultSetMapper<T> resultSetMapper = new ResultSetMapper<>();
        List<T> result = new ArrayList<>();

        Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            Connection conn = em.getConnection();

            CallableStatement stmt = conn.prepareCall("{ ? = call " + storedFunctionWithArguments + " }");
            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            for(Map.Entry<Integer, Object> entry : arguments.entrySet()) {
                int parameterIndex = entry.getKey() + 1;
                Object value = entry.getValue();

                stmt.setObject(parameterIndex, value);
            }

            stmt.execute();
            ResultSet resultSet = (ResultSet)stmt.getObject (1);

            result = resultSetMapper.mapResultSetToObject(resultSet, outputClass, generateIds ? uniqueNumbers : null);

            resultSet.close();
            tx.commit();
        }
        catch (JPQLException | SQLException e) {
            e.printStackTrace();
        } finally {
            tx.end();
        }

        return result == null ? new ArrayList<>() : result;
    }

    public List<T> fetchData(String storedFunctionWithArguments, Class outputClass, HashMap<Integer, Object> arguments) {
        return fetchData(storedFunctionWithArguments, outputClass, arguments, false);
    }

    /**
     * Выполняет хранимую процедуру с указанными параметрами без возвращаемых значений.
     * @param storedFunctionWithArguments Строка с вопросами вместо аргументов. Пример: "decanatuser.tr_work_pkg.get_facultet(?, ?)"
     * @param arguments Словарь из аргументов. Ключ -- номер аргумента (начинается с 1), значение -- подставляемое на его место значение.
     */
    public void execute(String storedFunctionWithArguments, HashMap<Integer, Object> arguments) {
        Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            Connection conn = em.getConnection();

            CallableStatement stmt = conn.prepareCall("{ call " + storedFunctionWithArguments + " }");

            for(Map.Entry<Integer, Object> entry : arguments.entrySet()) {
                int parameterIndex = entry.getKey();
                Object value = entry.getValue();

                stmt.setObject(parameterIndex, value);
            }

            stmt.execute();
            tx.commit();
        }
        catch (JPQLException | SQLException e) {
            e.printStackTrace();
        } finally {
            tx.end();
        }
    }

}