package com.company.rgr.service.helpers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.haulmont.cuba.core.app.UniqueNumbersAPI;
import org.apache.commons.beanutils.BeanUtils;

public class ResultSetMapper<T> {
    @SuppressWarnings("unchecked")
    public List<T> mapResultSetToObject(ResultSet rs, Class outputClass, UniqueNumbersAPI uniqueNumbers) {
        List<T> outputList = null;
        try {
            // make sure resultset is not null
            if (rs == null) {
                return null;
            }
            // check if outputClass has 'Entity' annotation
            if (!outputClass.isAnnotationPresent(Entity.class)) {
                return null;
            }

            // get the resultset metadata
            ResultSetMetaData rsmd = rs.getMetaData();
            // get all the attributes of outputClass
            Field[] fields = outputClass.getDeclaredFields();
            while (rs.next()) {
                T bean = (T) outputClass.newInstance();
                for (int _iterator = 0; _iterator < rsmd.getColumnCount(); _iterator++) {
                    // getting the SQL column name
                    String columnName = rsmd.getColumnName(_iterator + 1);

                    // reading the value of the SQL column
                    Object columnValue = rs.getObject(_iterator + 1);

                    // iterating over outputClass attributes to check if any attribute has 'Column' annotation with matching 'name' value
                    for (Field field : fields) {
                        if (field.isAnnotationPresent(Column.class)) {
                            Column column = field.getAnnotation(Column.class);
                            if (column.name().equalsIgnoreCase(columnName) && columnValue != null) {
                                BeanUtils.setProperty(bean, field.getName(), columnValue);
                                break;
                            }
                        }
                    }
                }
                if (outputList == null) {
                    outputList = new ArrayList<>();
                }

                if (uniqueNumbers != null) {
                    BeanUtils.setProperty(bean, "id", uniqueNumbers.getNextNumber(outputClass.getSimpleName()));
                }
                outputList.add(bean);
            }
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException | SQLException e) {
            e.printStackTrace();
        }
        return outputList;
    }

    public List<T> mapResultSetToObject(ResultSet rs, Class outputClass) {
        return mapResultSetToObject(rs, outputClass, null);
    }
}
