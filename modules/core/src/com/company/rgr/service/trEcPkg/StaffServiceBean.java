package com.company.rgr.service.trEcPkg;

import com.company.rgr.entity.fromOracle.trEcPkg.StaffPerson;
import com.company.rgr.service.EntityService;
import com.company.rgr.service.trEcPkg.StaffService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(StaffService.NAME)
public class StaffServiceBean implements StaffService {

    @Inject
    private EntityService<StaffPerson> entityService;

    /**
     * Возвращает список сотрудников.
     * @param chairId ID кафедры.
     * @param year Год.
     * @param onlyLoaded Только с учебной нагрузкой.
     * @param onlyPps Только ППС и НИЧ.
     * @return Список сотрудников.
     */
    public List<StaffPerson> getStaff(Object chairId, Object year, Object onlyLoaded, Object onlyPps) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, chairId);
        arguments.put(2, year);
        arguments.put(3, onlyLoaded);
        arguments.put(4, onlyPps);

        return entityService.fetchData("decanatuser.tr_ec_pkg.get_staff2(?, ?, ?, ?)", StaffPerson.class, arguments, true);
    }

    /**
     * Устанавливает статус эффективного контракта.
     * @param chairId ID кафедры.
     * @param personId ID сотрудника.
     * @param status Статус: 1 - завершить, 0 - возобновить, 2 - принять, 3 - отклонить.
     */
    public void setStatus(Object chairId, Object personId, Object status) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, chairId);
        arguments.put(2, personId);
        arguments.put(3, status);

        entityService.execute("decanatuser.tr_work_pkg.set_status_ec(?, ?, ?)", arguments);
    }

}