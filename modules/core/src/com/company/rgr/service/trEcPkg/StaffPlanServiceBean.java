package com.company.rgr.service.trEcPkg;

import com.company.rgr.entity.fromOracle.trEcPkg.StaffPlan;
import com.company.rgr.service.EntityService;
import com.company.rgr.service.trEcPkg.StaffPlanService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;

@Service(StaffPlanService.NAME)
public class StaffPlanServiceBean implements StaffPlanService {

    @Inject
    private EntityService<StaffPlan> entityService;

    /**
     * Возвращает план сотрудника.
     * @param chairId ID кафедры.
     * @param personId ID сотрудника.
     * @param year Год.
     * @param onlyFilled Только заполненные данные.
     * @return План сотрудника.
     */
    public List<StaffPlan> getStaffPlan(Object chairId, Object personId, Object year, Object onlyFilled) {
        HashMap<Integer, Object> arguments = new HashMap<>();
        arguments.put(1, chairId);
        arguments.put(2, personId);
        arguments.put(3, year);
        arguments.put(4, onlyFilled);

        return entityService.fetchData("decanatuser.tr_ec_pkg.get_staff_plan(?, ?, ?, ?)", StaffPlan.class, arguments, true);
    }

}