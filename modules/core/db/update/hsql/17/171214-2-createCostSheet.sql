alter table RGR_COST_SHEET add constraint FK_RGR_COST_SHEET_WORK foreign key (WORK_ID) references RGR_WORK(ID);
alter table RGR_COST_SHEET add constraint FK_RGR_COST_SHEET_MATERIAL foreign key (MATERIAL_ID) references RGR_MATERIAL(ID);
create index IDX_RGR_COST_SHEET_WORK on RGR_COST_SHEET (WORK_ID);
create index IDX_RGR_COST_SHEET_MATERIAL on RGR_COST_SHEET (MATERIAL_ID);
