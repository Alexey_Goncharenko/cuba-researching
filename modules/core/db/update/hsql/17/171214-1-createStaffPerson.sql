create table RGR_STAFF_PERSON (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(50) not null,
    PASSPORT_DATA varchar(100) not null,
    LAST_NAME varchar(50) not null,
    PATRONYMIC varchar(50),
    --
    primary key (ID)
);
