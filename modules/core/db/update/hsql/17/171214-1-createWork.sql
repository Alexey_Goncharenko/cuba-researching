create table RGR_WORK (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    BRIGADE_ID varchar(36) not null,
    DESCRIPTION varchar(100),
    TYPE_ID varchar(36) not null,
    OBJECT_ID varchar(36) not null,
    PLAN_BEGIN_DATE timestamp not null,
    PLAN_END_DATE timestamp not null,
    FACT_BEGIN_DATE timestamp,
    FACT_END_DATE timestamp,
    --
    primary key (ID)
);
