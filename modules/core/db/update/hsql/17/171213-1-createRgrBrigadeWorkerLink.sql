create table RGR_BRIGADE_WORKER_LINK (
    BRIGADE_ID varchar(36) not null,
    WORKER_ID varchar(36) not null,
    primary key (BRIGADE_ID, WORKER_ID)
);
