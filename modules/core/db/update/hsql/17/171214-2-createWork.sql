alter table RGR_WORK add constraint FK_RGR_WORK_BRIGADE foreign key (BRIGADE_ID) references RGR_BRIGADE(ID);
alter table RGR_WORK add constraint FK_RGR_WORK_TYPE foreign key (TYPE_ID) references RGR_WORK_TYPE(ID);
alter table RGR_WORK add constraint FK_RGR_WORK_OBJECT foreign key (OBJECT_ID) references RGR_OBJECT(ID);
create index IDX_RGR_WORK_BRIGADE on RGR_WORK (BRIGADE_ID);
create index IDX_RGR_WORK_TYPE on RGR_WORK (TYPE_ID);
create index IDX_RGR_WORK_OBJECT on RGR_WORK (OBJECT_ID);
