create table RGR_WORKER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(50) not null,
    LAST_NAME varchar(50) not null,
    PATRONYMIC varchar(50),
    PASSPORT_DATA varchar(255) not null,
    --
    primary key (ID)
);
