alter table RGR_SECTION add constraint FK_RGR_SECTION_CHIEF foreign key (CHIEF_ID) references RGR_STAFF_PERSON(ID);
create index IDX_RGR_SECTION_CHIEF on RGR_SECTION (CHIEF_ID);
