create table RGR_COST_SHEET (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    WORK_ID varchar(36) not null,
    MATERIAL_ID varchar(36) not null,
    PLAN_EXPENSE bigint not null,
    FACT_EXPENSE bigint,
    --
    primary key (ID)
);
