alter table RGR_BRIGADE_WORKER_LINK add constraint FK_BRIWOR_BRIGADE foreign key (BRIGADE_ID) references RGR_BRIGADE(ID);
alter table RGR_BRIGADE_WORKER_LINK add constraint FK_BRIWOR_WORKER foreign key (WORKER_ID) references RGR_WORKER(ID);
