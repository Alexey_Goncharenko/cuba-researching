create table RGR_OBJECT (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50),
    --
    NAME varchar2(50) not null,
    DESCRIPTION varchar2(255),
    SECTION_ID varchar2(32) not null,
    --
    primary key (ID)
)^
