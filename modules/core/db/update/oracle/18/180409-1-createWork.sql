create table RGR_WORK (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50),
    --
    BRIGADE_ID varchar2(32) not null,
    DESCRIPTION varchar2(100),
    TYPE_ID varchar2(32) not null,
    OBJECT_ID varchar2(32) not null,
    PLAN_BEGIN_DATE timestamp not null,
    PLAN_END_DATE timestamp not null,
    FACT_BEGIN_DATE timestamp,
    FACT_END_DATE timestamp,
    --
    primary key (ID)
)^
