create table RGR_COST_SHEET (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50),
    --
    WORK_ID varchar2(32) not null,
    MATERIAL_ID varchar2(32) not null,
    PLAN_EXPENSE number(19) not null,
    FACT_EXPENSE number(19),
    --
    primary key (ID)
)^
