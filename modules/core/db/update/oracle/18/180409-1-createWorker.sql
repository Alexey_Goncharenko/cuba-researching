create table RGR_WORKER (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50),
    --
    FIRST_NAME varchar2(50) not null,
    LAST_NAME varchar2(50) not null,
    PATRONYMIC varchar2(50),
    PASSPORT_DATA varchar2(255) not null,
    --
    primary key (ID)
)^
