create table RGR_STAFF_PERSON (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50),
    --
    FIRST_NAME varchar2(50) not null,
    PASSPORT_DATA varchar2(100) not null,
    LAST_NAME varchar2(50) not null,
    PATRONYMIC varchar2(50),
    --
    primary key (ID)
)^
