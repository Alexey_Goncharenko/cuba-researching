-- begin RGR_WORKER
create unique index IDX_RGR_WORKER_UNIQ_PASSPORT_DATA on RGR_WORKER (PASSPORT_DATA) ^
-- end RGR_WORKER
-- begin RGR_BRIGADE
alter table RGR_BRIGADE add constraint FK_RGR_BRIGADE_BRIGADIER foreign key (BRIGADIER_ID) references RGR_WORKER(ID)^
create unique index IDX_RGR_BRIGADE_UNIQ_NAME on RGR_BRIGADE (NAME) ^
create index IDX_RGR_BRIGADE_BRIGADIER on RGR_BRIGADE (BRIGADIER_ID)^
-- end RGR_BRIGADE
-- begin RGR_BRIGADE_WORKER_LINK
alter table RGR_BRIGADE_WORKER_LINK add constraint FK_BRIWOR_BRIGADE foreign key (BRIGADE_ID) references RGR_BRIGADE(ID)^
alter table RGR_BRIGADE_WORKER_LINK add constraint FK_BRIWOR_WORKER foreign key (WORKER_ID) references RGR_WORKER(ID)^
-- end RGR_BRIGADE_WORKER_LINK
-- begin RGR_SECTION
alter table RGR_SECTION add constraint FK_RGR_SECTION_CHIEF foreign key (CHIEF_ID) references RGR_STAFF_PERSON(ID)^
create index IDX_RGR_SECTION_CHIEF on RGR_SECTION (CHIEF_ID)^
-- end RGR_SECTION
-- begin RGR_OBJECT
alter table RGR_OBJECT add constraint FK_RGR_OBJECT_SECTION foreign key (SECTION_ID) references RGR_SECTION(ID)^
create index IDX_RGR_OBJECT_SECTION on RGR_OBJECT (SECTION_ID)^
-- end RGR_OBJECT
-- begin RGR_WORK
alter table RGR_WORK add constraint FK_RGR_WORK_BRIGADE foreign key (BRIGADE_ID) references RGR_BRIGADE(ID)^
alter table RGR_WORK add constraint FK_RGR_WORK_TYPE foreign key (TYPE_ID) references RGR_WORK_TYPE(ID)^
alter table RGR_WORK add constraint FK_RGR_WORK_OBJECT foreign key (OBJECT_ID) references RGR_OBJECT(ID)^
create index IDX_RGR_WORK_BRIGADE on RGR_WORK (BRIGADE_ID)^
create index IDX_RGR_WORK_TYPE on RGR_WORK (TYPE_ID)^
create index IDX_RGR_WORK_OBJECT on RGR_WORK (OBJECT_ID)^
-- end RGR_WORK
-- begin RGR_MATERIAL
create unique index IDX_RGR_MATERIAL_UNIQ_NAME on RGR_MATERIAL (NAME) ^
-- end RGR_MATERIAL
-- begin RGR_COST_SHEET
alter table RGR_COST_SHEET add constraint FK_RGR_COST_SHEET_WORK foreign key (WORK_ID) references RGR_WORK(ID)^
alter table RGR_COST_SHEET add constraint FK_RGR_COST_SHEET_MATERIAL foreign key (MATERIAL_ID) references RGR_MATERIAL(ID)^
create index IDX_RGR_COST_SHEET_WORK on RGR_COST_SHEET (WORK_ID)^
create index IDX_RGR_COST_SHEET_MATERIAL on RGR_COST_SHEET (MATERIAL_ID)^
-- end RGR_COST_SHEET
