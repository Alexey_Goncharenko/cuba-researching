-- begin RGR_WORKER
create table RGR_WORKER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(50) not null,
    LAST_NAME varchar(50) not null,
    PATRONYMIC varchar(50),
    PASSPORT_DATA varchar(255) not null,
    --
    primary key (ID)
)^
-- end RGR_WORKER
-- begin RGR_BRIGADE
create table RGR_BRIGADE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    BRIGADIER_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end RGR_BRIGADE
-- begin RGR_BRIGADE_WORKER_LINK
create table RGR_BRIGADE_WORKER_LINK (
    BRIGADE_ID varchar(36) not null,
    WORKER_ID varchar(36) not null,
    primary key (BRIGADE_ID, WORKER_ID)
)^
-- end RGR_BRIGADE_WORKER_LINK
-- begin RGR_STAFF_PERSON
create table RGR_STAFF_PERSON (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(50) not null,
    PASSPORT_DATA varchar(100) not null,
    LAST_NAME varchar(50) not null,
    PATRONYMIC varchar(50),
    --
    primary key (ID)
)^
-- end RGR_STAFF_PERSON
-- begin RGR_SECTION
create table RGR_SECTION (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    CHIEF_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end RGR_SECTION
-- begin RGR_OBJECT
create table RGR_OBJECT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    DESCRIPTION varchar(255),
    SECTION_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end RGR_OBJECT
-- begin RGR_WORK
create table RGR_WORK (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    BRIGADE_ID varchar(36) not null,
    DESCRIPTION varchar(100),
    TYPE_ID varchar(36) not null,
    OBJECT_ID varchar(36) not null,
    PLAN_BEGIN_DATE timestamp not null,
    PLAN_END_DATE timestamp not null,
    FACT_BEGIN_DATE timestamp,
    FACT_END_DATE timestamp,
    --
    primary key (ID)
)^
-- end RGR_WORK
-- begin RGR_WORK_TYPE
create table RGR_WORK_TYPE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    --
    primary key (ID)
)^
-- end RGR_WORK_TYPE
-- begin RGR_MATERIAL
create table RGR_MATERIAL (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    --
    primary key (ID)
)^
-- end RGR_MATERIAL
-- begin RGR_COST_SHEET
create table RGR_COST_SHEET (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    WORK_ID varchar(36) not null,
    MATERIAL_ID varchar(36) not null,
    PLAN_EXPENSE bigint not null,
    FACT_EXPENSE bigint,
    --
    primary key (ID)
)^
-- end RGR_COST_SHEET
