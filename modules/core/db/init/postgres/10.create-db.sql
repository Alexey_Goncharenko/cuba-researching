-- begin RGR_WORKER
create table RGR_WORKER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(50) not null,
    LAST_NAME varchar(50) not null,
    PATRONYMIC varchar(50),
    PASSPORT_DATA varchar(255) not null,
    --
    primary key (ID)
)^
-- end RGR_WORKER
-- begin RGR_BRIGADE
create table RGR_BRIGADE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    BRIGADIER_ID uuid not null,
    --
    primary key (ID)
)^
-- end RGR_BRIGADE
-- begin RGR_STAFF_PERSON
create table RGR_STAFF_PERSON (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(50) not null,
    PASSPORT_DATA varchar(100) not null,
    LAST_NAME varchar(50) not null,
    PATRONYMIC varchar(50),
    --
    primary key (ID)
)^
-- end RGR_STAFF_PERSON
-- begin RGR_SECTION
create table RGR_SECTION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    CHIEF_ID uuid not null,
    --
    primary key (ID)
)^
-- end RGR_SECTION
-- begin RGR_OBJECT
create table RGR_OBJECT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    DESCRIPTION varchar(255),
    SECTION_ID uuid not null,
    --
    primary key (ID)
)^
-- end RGR_OBJECT
-- begin RGR_WORK
create table RGR_WORK (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    BRIGADE_ID uuid not null,
    DESCRIPTION varchar(100),
    TYPE_ID uuid not null,
    OBJECT_ID uuid not null,
    PLAN_BEGIN_DATE timestamp not null,
    PLAN_END_DATE timestamp not null,
    FACT_BEGIN_DATE timestamp,
    FACT_END_DATE timestamp,
    --
    primary key (ID)
)^
-- end RGR_WORK
-- begin RGR_WORK_TYPE
create table RGR_WORK_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    --
    primary key (ID)
)^
-- end RGR_WORK_TYPE
-- begin RGR_MATERIAL
create table RGR_MATERIAL (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(50) not null,
    --
    primary key (ID)
)^
-- end RGR_MATERIAL
-- begin RGR_COST_SHEET
create table RGR_COST_SHEET (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    WORK_ID uuid not null,
    MATERIAL_ID uuid not null,
    PLAN_EXPENSE bigint not null,
    FACT_EXPENSE bigint,
    --
    primary key (ID)
)^
-- end RGR_COST_SHEET
-- begin RGR_BRIGADE_WORKER_LINK
create table RGR_BRIGADE_WORKER_LINK (
    BRIGADE_ID uuid,
    WORKER_ID uuid,
    primary key (BRIGADE_ID, WORKER_ID)
)^
-- end RGR_BRIGADE_WORKER_LINK
-- begin RGR_TEST_ENTITY
create table RGR_TEST_ENTITY (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    CODE varchar(255) not null,
    COUNT_ integer,
    --
    primary key (ID)
)^
-- end RGR_TEST_ENTITY
-- begin RGR_TEST_ENTITY1
create table RGR_TEST_ENTITY1 (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    CODE varchar(255) not null,
    COUNT_ integer,
    --
    primary key (ID)
)^
-- end RGR_TEST_ENTITY1
