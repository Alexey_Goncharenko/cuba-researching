package com.company.rgr.web.timeSheet.customdatasources;

import com.company.rgr.entity.fromOracle.trWorkPkg.SrcLine;
import com.company.rgr.service.trWorkPkg.SrcLineService;
import com.haulmont.cuba.core.global.AppBeans;

import java.util.Collection;
import java.util.Map;

import com.haulmont.cuba.gui.data.impl.CustomCollectionDatasource;

public class SrcLineDatasource extends CustomCollectionDatasource<SrcLine, Long> {

    private SrcLineService srcLineService = AppBeans.get(SrcLineService.NAME);

    @Override
    protected Collection<SrcLine> getEntities(Map<String, Object> params) {
        return srcLineService.getSrcLines(
                params.getOrDefault("year", null),
                params.getOrDefault("chairId", null),
                params.getOrDefault("branchId", null),
                params.getOrDefault("linkedPersonId", null)
        );
    }
}

