package com.company.rgr.web.timeSheet.customdatasources;

import com.company.rgr.entity.fromOracle.trEcPkg.StaffPerson;
import com.company.rgr.service.trEcPkg.StaffService;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.gui.data.impl.CustomCollectionDatasource;

import java.util.Collection;
import java.util.Map;

public class StaffDatasource extends CustomCollectionDatasource<StaffPerson, Long> {

    private StaffService staffService = AppBeans.get(StaffService.NAME);

    @Override
    protected Collection<StaffPerson> getEntities(Map<String, Object> params) {
        return staffService.getStaff(
                params.getOrDefault("chairId", null),
                params.getOrDefault("year", null),
                params.getOrDefault("onlyLoaded", null),
                params.getOrDefault("onlyPps", null)
        );
    }
}

