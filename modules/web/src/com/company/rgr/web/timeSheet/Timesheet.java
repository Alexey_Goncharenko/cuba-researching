package com.company.rgr.web.timeSheet;

import com.company.rgr.entity.fromOracle.trEcPkg.StaffPerson;
import com.company.rgr.entity.fromOracle.trEcPkg.StaffPlan;
import com.company.rgr.entity.fromOracle.trWorkPkg.*;
import com.company.rgr.service.*;

import com.company.rgr.service.trEcPkg.StaffService;
import com.company.rgr.service.trWorkPkg.*;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.data.CollectionDatasource;

import com.haulmont.cuba.gui.data.GroupDatasource;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;

import javax.inject.Inject;
import java.util.*;

public class Timesheet extends AbstractWindow {
    @Inject
    private ComponentsFactory componentsFactory;

    @Inject
    private ChairService chairService;
    @Inject
    private CommonService commonService;
    @Inject
    private FacultyService facultyService;
    @Inject
    private BranchService branchService;
    @Inject
    private LinkedPersonService linkedPersonService;
    @Inject
    private PersonService personService;
    @Inject
    private DistributionLineService distributionLineService;
    @Inject
    private StaffService staffService;
    @Inject
    private DataGrid<SrcLine> dgSrcLines;
    @Inject
    private DataGrid<DistributionLine> dgDistributionLines;
    @Inject
    private DataGrid<StaffPerson> dgStaff;
    @Inject
    private GroupTable<StaffPlan> dgStaffPlan;
    @Inject
    private CollectionDatasource<SrcLine, Long> srcLineDs;
    @Inject
    private CollectionDatasource<DistributionLine, Long> distrLineDs;
    @Inject
    private CollectionDatasource<StaffPerson, Long> staffDs;
    @Inject
    private GroupDatasource<StaffPlan, Long> staffPlanDs;
    @Inject
    private LookupField cbChair;
    @Inject
    private LookupField cbFaculty;
    @Inject
    private LookupField cbBranch;
    @Inject
    private LookupField cbTeacher;
    @Inject
    private TextField tbYear;
    @Inject
    private CheckBox cbOnlyFromChair;
    @Inject
    private CheckBox cbOnlyLoaded;
    @Inject
    private CheckBox cbOnlyPps;
    @Inject
    private CheckBox cbOnlyFilled;

    @Override
    public void init(Map<String, Object> params) {
        commonService.authenticate("test", "test");

        setDefaultYear();
        updateChairs();
        updateFaculties();
        updateBranches();
        updateTeachers();

        updateSrcLines();
        updateStaff();

        initDistributionLinesGrid();
        initStaffPlanGrid();
        updateDistributionLines(null);

        initEventListeners();
    }

    private void initEventListeners() {
        tbYear.addValueChangeListener(e -> {
            updateChairs();
            updateFaculties();
            updateBranches();
            updateTeachers();

            updateSrcLines();
            updateStaff();
        });

        cbChair.addValueChangeListener(e -> {
            updateFaculties();
            updateBranches();
            updateTeachers();

            updateSrcLines();
            updateStaff();
        });

        cbFaculty.addValueChangeListener(e -> {
            updateBranches();

            updateSrcLines();
        });

        cbBranch.addValueChangeListener(e -> {
            updateSrcLines();
        });

        cbTeacher.addValueChangeListener(e -> {
            updateSrcLines();
        });

        dgSrcLines.addItemClickListener(e -> {
            updateDistributionLines(e.getItem());
        });

        cbOnlyFromChair.addValueChangeListener(e -> {
            initDistributionLinesEditor();
        });

        cbOnlyLoaded.addValueChangeListener(e -> {
            updateStaff();
        });

        cbOnlyPps.addValueChangeListener(e -> {
            updateStaff();
        });

        dgStaff.addItemClickListener(e -> {
            updateStaffPlan();
        });

        cbOnlyFilled.addValueChangeListener(e -> {
            updateStaffPlan();
        });
    }

    private Object getCurrentYear() {
        return tbYear.getValue();
    }
    private Object getCurrentChairId() {
        return cbChair.getValue();
    }
    private Object getCurrentFacultyId() {
        return cbFaculty.getValue();
    }
    private Object getCurrentTeacherId() {
        return cbTeacher.getValue();
    }
    private Object getCurrentBranchId() {
        return cbBranch.getValue();
    }
    private Object getOnlyFromChairValue() {
        return cbOnlyFromChair.getValue() ? 1 : 0;
    }
    private Object getOnlyLoaded() {
        return cbOnlyLoaded.getValue() ? 1 : 0;
    }
    private Object getOnlyPps() {
        return cbOnlyPps.getValue() ? 1 : 0;
    }
    private Object getCurrentPK() {
        SrcLine selected = dgSrcLines.getSingleSelected();
        return selected == null ? null : selected.getPk();
    }
    private Object getCurrentType() {
        SrcLine selected = dgSrcLines.getSingleSelected();
        return selected == null ? null : selected.getType();
    }
    private Object getStaffPersonId() {
        StaffPerson selected = dgStaff.getSingleSelected();
        return selected == null ? null : selected.getPersonId();
    }
    private Object getOnlyFilled() {
        return cbOnlyFilled.getValue() ? 1 : 0;
    }

    private void setDefaultYear() {
        int year = commonService.getUsefulYear();
        tbYear.setValue(year);
    }

    private void updateChairs() {
        List<Chair> result = chairService.getChairs(getCurrentYear());
        if (result == null) {
            return;
        }

        Map<String, Object> map = new LinkedHashMap<>();

        for (Chair item : result) {
            map.put(item.getName(), item.getId());
        }
        cbChair.setOptionsMap(map);
        cbChair.setValue(result.size() > 0 ? result.get(0).getId() : null);
    }

    private void updateFaculties() {
        List<Faculty> result = facultyService.getFaculties(getCurrentYear(), getCurrentChairId());
        if (result == null) {
            return;
        }

        Map<String, Object> map = new LinkedHashMap<>();

        for (Faculty item : result) {
            map.put(item.getName(), item.getId());
        }
        cbFaculty.setOptionsMap(map);
        cbFaculty.setValue(result.size() > 0 ? result.get(0).getId() : null);
    }

    private void updateTeachers() {
        List<LinkedPerson> result = linkedPersonService.getLinkedPersons(getCurrentYear(), getCurrentChairId());
        if (result == null) {
            return;
        }

        Map<String, Object> map = new LinkedHashMap<>();

        for (LinkedPerson item : result) {
            map.put(item.getName(), item.getId());
        }
        cbTeacher.setOptionsMap(map);
        cbTeacher.setValue(null);
    }

    private void updateBranches() {
        List<Branch> result = branchService.getBranches(getCurrentYear(), getCurrentChairId(), getCurrentFacultyId());
        if (result == null) {
            return;
        }

        Map<String, Object> map = new LinkedHashMap<>();

        for (Branch item : result) {
            map.put(item.getName(), item.getId());
        }
        cbBranch.setOptionsMap(map);
        cbBranch.setValue(result.size() > 0 ? result.get(0).getId() : null);
    }

    private void updateSrcLines() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("year", getCurrentYear());
        params.put("chairId", getCurrentChairId());
        params.put("branchId", getCurrentBranchId());
        params.put("linkedPersonId", getCurrentTeacherId());

        srcLineDs.refresh(params);
    }

    private void updateDistributionLines(SrcLine selected) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("pk", selected == null ? null : selected.getPk());
        params.put("type", selected == null ? null : selected.getType());

        distrLineDs.refresh(params);
    }

    private void initDistributionHeader() {
        DataGrid.HeaderRow headerRow = dgDistributionLines.prependHeaderRow();
        DataGrid.HeaderCell header1 = headerRow.join("group", "work", "weeksCount", "studentsCount", "sgNum", "hoursFor");
        header1.setText("Поручение");
        header1.setStyleName("bold");

        DataGrid.HeaderCell header2 = headerRow.join("person_id", "weeksCountL", "studentsCountL", "hours", "hoursCurrent", "lecturerPercents", "lecturerHours");
        header2.setText("Распределение");
    }

    private Map<Long, String> distLinesById = new LinkedHashMap<>();
    private Map<String, Long> distLinesByName = new LinkedHashMap<>();
    private void initDistributionLinesEditor() {
        List<Person> result = personService.getPersons(getCurrentChairId(), getOnlyFromChairValue());
        if (result == null) {
            return;
        }

        for (Person item : result) {
            distLinesByName.put(item.getName(), item.getId());
            distLinesById.put(item.getId(), item.getName());
        }

        DataGrid.Column column = dgDistributionLines.getColumnNN("person_id");

        column.setEditorFieldGenerator((datasource, property) -> {
            LookupField lookupField = componentsFactory.createComponent(LookupField.class);
            lookupField.setDatasource(datasource, property);
            lookupField.setOptionsMap(distLinesByName);

            return lookupField;
        });

        column.setConverter(new DataGrid.Converter<String, Long>() {
            @Override
            public Long convertToModel(String value, Class<? extends Long> targetType, Locale locale) {
                return distLinesByName.get(value);
            }

            @Override
            public String convertToPresentation(Long value, Class<? extends String> targetType, Locale locale) {
                return distLinesById.get(value);
            }

            @Override
            public Class<Long> getModelType() {
                return Long.class;
            }

            @Override
            public Class<String> getPresentationType() {
                return String.class;
            }
        });
    }

    private void initDistributionLinesEventListeners() {
        dgDistributionLines.addEditorPostCommitListener(e -> {
            Long id = (Long)dgDistributionLines.getEditedItemId();
            DistributionLine line = distrLineDs.getItem(id);

            distributionLineService.editDistributionLine(
                    line.getFkPersonLine(),
                    line.getPk(),
                    getCurrentType(),
                    line.getFkTrWorkChair(),
                    line.getPersonId(),
                    line.getWeeksCountL(),
                    line.getStudentsCountL(),
                    line.getFkTypeStudyWork(),
                    line.getFkGroup(),
                    line.getSgNum(),
                    line.getHours(),
                    line.getHoursCurrent(),
                    line.getLecturerHours()
            );
        });
    }

    private void initDistributionLinesGrid() {
        initDistributionHeader();
        initDistributionLinesEditor();
        initDistributionLinesEventListeners();
    }

    public void onCopy(Component source) { }

    /* Вкладка "Эффективный контракт" */

    private void updateStaff() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("chairId", getCurrentChairId());
        params.put("year", getCurrentYear());
        params.put("onlyLoaded", getOnlyLoaded());
        params.put("onlyPps", getOnlyPps());

        staffDs.refresh(params);
    }

    private void initStaffPlanStylesProvider() {
        dgStaffPlan.setStyleProvider((entity, property) -> {
            if (property == null) {
                return null;
            }

            if (
                entity.getIsComplex() == 1 &&
                (property.equals("value") || property.equals("value2") || property.equals("coefficient"))
            ) {
                return "cell--purple";
            }

            if (
                entity.getIsComplex() == 0 &&
                (property.equals("value") || property.equals("value2"))
            ) {
                return "cell--green";
            }

            if (entity.getIsCoAuthor() == 0 && property.equals("value2")) {
                return "cell--gray";
            }

            return null;
        });
    }

    private void initStaffPlanGrid() {
        initStaffPlanStylesProvider();
    }

    private void updateStaffPlan() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("chairId", getCurrentChairId());
        params.put("personId", getStaffPersonId());
        params.put("year", getCurrentYear());
        params.put("onlyFilled", getOnlyFilled());

        staffPlanDs.refresh(params);
    }

    public void onFinish(Component source) {
        StaffPerson selected = dgStaff.getSingleSelected();
        if (selected == null) {
            return;
        }

        staffService.setStatus(selected.getChairId(), selected.getPersonId(), 1);
        updateStaff();
    }

    public void onResume(Component source) {
        StaffPerson selected = dgStaff.getSingleSelected();
        if (selected == null) {
            return;
        }

        staffService.setStatus(selected.getChairId(), selected.getPersonId(), 0);
        updateStaff();
    }

    public void onAccept(Component source) {
        StaffPerson selected = dgStaff.getSingleSelected();
        if (selected == null) {
            return;
        }

        staffService.setStatus(selected.getChairId(), selected.getPersonId(), 2);
        updateStaff();
    }

    public void onReject(Component source) {
        StaffPerson selected = dgStaff.getSingleSelected();
        if (selected == null) {
            return;
        }

        staffService.setStatus(selected.getChairId(), selected.getPersonId(), 3);
        updateStaff();
    }
}