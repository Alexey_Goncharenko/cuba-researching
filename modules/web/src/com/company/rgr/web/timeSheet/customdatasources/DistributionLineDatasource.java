package com.company.rgr.web.timeSheet.customdatasources;

import com.company.rgr.entity.fromOracle.trWorkPkg.DistributionLine;
import com.company.rgr.service.trWorkPkg.DistributionLineService;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.gui.data.impl.CustomCollectionDatasource;

import java.util.Collection;
import java.util.Map;

public class DistributionLineDatasource extends CustomCollectionDatasource<DistributionLine, Long> {

    private DistributionLineService distributionLineService = AppBeans.get(DistributionLineService.NAME);

    @Override
    protected Collection<DistributionLine> getEntities(Map<String, Object> params) {
        return distributionLineService.getDistributionLines(
                params.getOrDefault("pk", null),
                params.getOrDefault("type", null)
        );
    }
}

