package com.company.rgr.web.timeSheet.customdatasources;

import com.company.rgr.entity.fromOracle.trEcPkg.StaffPlan;
import com.company.rgr.service.trEcPkg.StaffPlanService;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.gui.data.impl.CustomGroupDatasource;

import java.util.Collection;
import java.util.Map;

public class StaffPlanDatasource extends CustomGroupDatasource<StaffPlan, Long> {

    private StaffPlanService staffPlanService = AppBeans.get(StaffPlanService.NAME);

    @Override
    protected Collection<StaffPlan> getEntities(Map<String, Object> params) {
        return staffPlanService.getStaffPlan(
                params.getOrDefault("chairId", null),
                params.getOrDefault("personId", null),
                params.getOrDefault("year", null),
                params.getOrDefault("onlyFilled", null)
        );
    }
}

