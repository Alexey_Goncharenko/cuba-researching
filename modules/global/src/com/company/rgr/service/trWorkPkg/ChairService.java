package com.company.rgr.service.trWorkPkg;


import com.company.rgr.entity.fromOracle.trWorkPkg.Chair;

import java.util.List;

public interface ChairService {
    String NAME = "rgr_ChairService";

    /**
     * Возвращает список кафедр.
     * @param year Год.
     * @return Список кафедр, упорядоченных по алфавиту.
     */
    List<Chair> getChairs(Object year);
}
