package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.Person;

import java.util.List;

public interface PersonService {
    String NAME = "rgr_PersonService";

    /**
     * Возвращает список преподавателей на указанной кафедре.
     * @param chairId ID кафедры.
     * @param checkboxValue 1, если только с кафедры; 0 иначе.
     * @return Список преподавателей.
     */
    List<Person> getPersons(Object chairId, Object checkboxValue);
}