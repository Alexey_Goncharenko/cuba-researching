package com.company.rgr.service.trWorkPkg;

import com.company.rgr.entity.fromOracle.trWorkPkg.Branch;

import java.util.List;

public interface BranchService {
    String NAME = "rgr_BranchService";

    /**
     * Возвращает список отделений для выбранных года, кафедры и факультета.
     * @param year Год.
     * @param chairId ID кафедры.
     * @param facultyId ID факультета.
     * @return Список отделений.
     */
    List<Branch> getBranches(Object year, Object chairId, Object facultyId);
}