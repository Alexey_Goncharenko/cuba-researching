package com.company.rgr.service.trWorkPkg;


import com.company.rgr.entity.fromOracle.trWorkPkg.DistributionLine;

import java.util.List;

public interface DistributionLineService {
    String NAME = "rgr_DistributionLineService";

    /**
     * Возвращает распределение нагрузи.
     * @param pk Primary key.
     * @param type Type.
     * @return Распределение нагрузки.
     */
    List<DistributionLine> getDistributionLines(Object pk, Object type);

    /**
     * Изменяет запись DistributionLine в соответствии с параметрами.
     */
    void editDistributionLine(
            Object fkPersonLine,
            Object pk,
            Object type,
            Object fkTrWorkChair,
            Object personId,
            Object weeksCountL,
            Object studentsCountL,
            Object fkTypeStudyWork,
            Object fkGroup,
            Object sgNum,
            Object hours,
            Object currentHours,
            Object lecturerHours
    );
}