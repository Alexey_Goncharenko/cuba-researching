package com.company.rgr.service.trWorkPkg;


import com.company.rgr.entity.fromOracle.trWorkPkg.SrcLine;

import java.util.List;

public interface SrcLineService {
    String NAME = "rgr_SrcLineService";

    /**
     * Возвращает нагрузку в указанном году я выбранных кафедры, отделения и преподавателя.
     * @param year Год.
     * @param chairId ID кафедры.
     * @param branchId ID отделения.
     * @param linkedPersonId ID преподавателя.
     * @return Нагрузка.
     */
    List<SrcLine> getSrcLines(Object year, Object chairId, Object branchId, Object linkedPersonId);
}