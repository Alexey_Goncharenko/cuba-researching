package com.company.rgr.service.trWorkPkg;


import com.company.rgr.entity.fromOracle.trWorkPkg.LinkedPerson;

import java.util.List;

public interface LinkedPersonService {
    String NAME = "rgr_LinkedPersonService";

    /**
     * Возвращает список преподавателей на указанной кафедре в указанном году.
     * @param year Год.
     * @param chairId ID кафедры.
     * @return Список преподавателей.
     */
    List<LinkedPerson> getLinkedPersons(Object year, Object chairId);
}