package com.company.rgr.service.trWorkPkg;


import com.company.rgr.entity.fromOracle.trWorkPkg.Faculty;

import java.util.List;

public interface FacultyService {
    String NAME = "rgr_FacultyService";

    /**
     * Возвращает список факультетов для выбранных года и кафедры.
     * @param year Год.
     * @param chairId ID кафедры.
     * @return Список факультетов.
     */
    List<Faculty> getFaculties(Object year, Object chairId);
}