package com.company.rgr.service;


public interface CommonService {
    String NAME = "rgr_CommonService";

    /**
     * Возвращает наиболее вероятно используемый в данный момент год.
     * @return Год.
     */
    int getUsefulYear();

    /**
     * Аутентифицирует пользователя в БД.
     * @param login Логин.
     * @param password Пароль.
     * @return Возвращает id пользователя.
     */
    int authenticate(String login, String password);
}