package com.company.rgr.service;


import java.util.HashMap;
import java.util.List;

public interface EntityService<T> {
    String NAME = "rgr_EntityService";

    /**
     * Забирает список из сущностей, возвращаемых хранимой процедурой через ref_cursor.
     * @param storedFunctionWithArguments Строка с вопросами вместо аргументов. Пример: "decanatuser.tr_work_pkg.get_facultet(?, ?)"
     * @param outputClass Класс объекта, элементы которого будут составлять список. Должен иметь аннтоацию @Entity, а поля должны иметь аннотации @Column(name = "COLNAME")
     * @param arguments Словарь из аргументов. Ключ -- номер аргумента (начинается с 1), значение -- подставляемое на его место значение.
     * @param generateIds True, если процедура не возвращает уникального ключа, и его необходимо генерировать.
     * @return Возвращает пустой или непустой List, всегда не null.
     */
    List<T> fetchData(String storedFunctionWithArguments, Class outputClass, HashMap<Integer, Object> arguments, boolean generateIds);

    List<T> fetchData(String storedFunctionWithArguments, Class outputClass, HashMap<Integer, Object> arguments);

    /**
     * Выполняет хранимую процедуру с указанными параметрами без возвращаемых значений.
     * @param storedFunctionWithArguments Строка с вопросами вместо аргументов. Пример: "decanatuser.tr_work_pkg.get_facultet(?, ?)"
     * @param arguments Словарь из аргументов. Ключ -- номер аргумента (начинается с 1), значение -- подставляемое на его место значение.
     */
    void execute(String storedFunctionWithArguments, HashMap<Integer, Object> arguments);
}