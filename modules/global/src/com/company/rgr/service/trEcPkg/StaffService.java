package com.company.rgr.service.trEcPkg;


import com.company.rgr.entity.fromOracle.trEcPkg.StaffPerson;

import java.util.List;

public interface StaffService {
    String NAME = "rgr_StaffService";

    /**
     * Возвращает список сотрудников.
     * @param chairId ID кафедры.
     * @param year Год.
     * @param onlyLoaded Только с учебной нагрузкой.
     * @param onlyPps Только ППС и НИЧ.
     * @return Список сотрудников.
     */
    List<StaffPerson> getStaff(Object chairId, Object year, Object onlyLoaded, Object onlyPps);

    /**
     * Устанавливает статус эффективного контракта.
     * @param chairId ID кафедры.
     * @param personId ID сотрудника.
     * @param status Статус: 1 - завершить, 0 - возобновить, 2 - принять, 3 - отклонить.
     */
    void setStatus(Object chairId, Object personId, Object status);
}