package com.company.rgr.service.trEcPkg;


import com.company.rgr.entity.fromOracle.trEcPkg.StaffPlan;

import java.util.List;

public interface StaffPlanService {
    String NAME = "rgr_StaffPlanService";

    /**
     * Возвращает план сотрудника.
     * @param chairId ID кафедры.
     * @param personId ID сотрудника.
     * @param year Год.
     * @param onlyFilled Только заполненные данные.
     * @return План сотрудника.
     */
    List<StaffPlan> getStaffPlan(Object chairId, Object personId, Object year, Object onlyFilled);
}