package com.company.rgr.entity.fromOracle.trWorkPkg;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Chair implements Serializable {
    @Id
    @Column(name = "ID")
    protected long id;

    @Column(name = "SNAME")
    protected String name;

    @Column(name = "SHORTNAME")
    protected String shortName;

    @Column(name = "ROLE")
    protected int role;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Chair)) {
            return false;
        }
        Chair other = (Chair) object;

        return (this.name != null || other.name == null) && (this.name == null || this.name.equals(other.name));
    }

    @Override
    public String toString() {
        return getId() + ": " + getName();
    }
}
