package com.company.rgr.entity.fromOracle.trEcPkg;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.chile.core.annotations.MetaProperty;
import java.util.Date;
import com.haulmont.cuba.core.entity.BaseLongIdEntity;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;

@NamePattern("%s|name")
@MetaClass(name = "rgr$StaffPerson")
@Entity
public class StaffPerson extends BaseLongIdEntity implements Updatable, Creatable {
    private static final long serialVersionUID = 6657109573189129394L;

    @MetaProperty
    protected Date createTs;

    @MetaProperty
    protected String createdBy;

    @MetaProperty
    protected Date updateTs;

    @MetaProperty
    protected String updatedBy;

    @MetaProperty
    @Column(name = "FIO")
    protected String name;

    @MetaProperty
    @Column(name = "POST")
    protected String post;

    @MetaProperty
    @Column(name = "NSTU_PKG")
    protected String category;

    @MetaProperty
    @Column(name = "STAVKA")
    protected Double rate;

    @MetaProperty
    @Column(name = "STATUS2")
    protected String status;

    @Column(name = "FK_TR_WORK_CHAIR")
    protected Long chairId;

    @Column(name = "ID")
    protected Long personId;

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPost() {
        return post;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getRate() {
        return rate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public Long getChairId() {
        return chairId;
    }

    public void setChairId(Long chairId) {
        this.chairId = chairId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }


}