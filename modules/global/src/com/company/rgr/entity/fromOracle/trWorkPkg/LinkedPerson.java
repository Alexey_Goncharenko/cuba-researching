package com.company.rgr.entity.fromOracle.trWorkPkg;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class LinkedPerson implements Serializable {
    @Id
    @Column(name = "ID")
    protected long id;

    @Column(name = "FIO")
    protected String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof LinkedPerson)) {
            return false;
        }
        LinkedPerson other = (LinkedPerson) object;

        return (this.name != null || other.name == null) && (this.name == null || this.name.equals(other.name));
    }

    @Override
    public String toString() {
        return getId() + ": " + getName();
    }
}
