package com.company.rgr.entity.fromOracle.trEcPkg;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.BaseLongIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;

@NamePattern("%s|name")
@MetaClass(name = "rgr$StaffPlan")
@Entity
public class StaffPlan extends BaseLongIdEntity {
    private static final long serialVersionUID = -8823035967602637545L;

    @MetaProperty
    @Column(name = "TYPE")
    protected String type;

    @MetaProperty
    @Column(name = "TNUM1")
    protected Integer num1;

    @MetaProperty
    @Column(name = "TNUM2")
    protected String num2;

    @MetaProperty
    @Column(name = "NAME")
    protected String name;

    @MetaProperty
    @Column(name = "VALUE_NORM")
    protected Long valueNorm;

    @MetaProperty
    @Column(name = "VALUE")
    protected Long value;

    @MetaProperty
    @Column(name = "VALUE2")
    protected Long value2;

    @MetaProperty
    @Column(name = "COEFFT")
    protected Double coefficient;

    @MetaProperty
    @Column(name = "RE")
    protected Long re;

    @MetaProperty
    @Column(name = "IS_COMPLEX")
    protected Long isComplex;

    @MetaProperty
    @Column(name = "IS_COAUTHOR")
    protected Long isCoAuthor;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setNum1(Integer num1) {
        this.num1 = num1;
    }

    public Integer getNum1() {
        return num1;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }

    public String getNum2() {
        return num2;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setValueNorm(Long valueNorm) {
        this.valueNorm = valueNorm;
    }

    public Long getValueNorm() {
        return valueNorm;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    public void setValue2(Long value2) {
        this.value2 = value2;
    }

    public Long getValue2() {
        return value2;
    }

    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }

    public Double getCoefficient() {
        return coefficient;
    }

    public void setRe(Long re) {
        this.re = re;
    }

    public Long getRe() {
        return re;
    }

    public Long getIsComplex() {
        return isComplex;
    }

    public void setIsComplex(Long isComplex) {
        this.isComplex = isComplex;
    }

    public Long getIsCoAuthor() {
        return isCoAuthor;
    }

    public void setIsCoAuthor(Long isCoAuthor) {
        this.isCoAuthor = isCoAuthor;
    }
}