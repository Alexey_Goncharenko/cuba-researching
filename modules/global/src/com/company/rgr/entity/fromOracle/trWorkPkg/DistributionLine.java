package com.company.rgr.entity.fromOracle.trWorkPkg;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.BaseLongIdEntity;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@SuppressWarnings("JpaDataSourceORMInspection")
@NamePattern("%s: %s|id,teacher")
@MetaClass(name = "rgr$DistributionLine")
@Entity
public class DistributionLine extends BaseLongIdEntity implements Serializable {
    private static final long serialVersionUID = -1952065557735463513L;

    @MetaProperty
    @Column(name = "PK")
    protected Long pk;

    @MetaProperty
    @Column(name = "ONE_GROUP")
    protected String group;

    @MetaProperty
    @Column(name = "TYPE_STUDY_WORK")
    protected String work;

    @MetaProperty
    @Column(name = "WEEKS_COUNT")
    protected Integer weeksCount;

    @MetaProperty
    @Column(name = "STUD_CNT")
    protected Integer studentsCount;

    @MetaProperty
    @Column(name = "SG_NUM")
    protected Integer sgNum;

    @MetaProperty
    @Column(name = "HOURS_FOR")
    protected Integer hoursFor;

    @MetaProperty
    @Column(name = "WEEKS_COUNT_L")
    protected Integer weeksCountL;

    @MetaProperty
    @Column(name = "STUDENTS_COUNT_L")
    protected Integer studentsCountL;

    @MetaProperty
    @Column(name = "HOURS")
    protected Integer hours;

    @MetaProperty
    @Column(name = "HOURS_CURR")
    protected Integer hoursCurrent;

    @MetaProperty
    @Column(name = "PRC_LECTOR")
    protected Integer lecturerPercents;

    @MetaProperty
    @Column(name = "HOURS_LECTOR")
    protected Integer lecturerHours;

    @MetaProperty
    @Column(name = "FK_PERSON_LINE")
    protected Integer fkPersonLine;

    @MetaProperty
    @Column(name = "FK_TR_WORK_CHAIR")
    protected Integer fkTrWorkChair;

    @MetaProperty
    @Column(name = "FK_TYPE_STUDY_WORK")
    protected Integer fkTypeStudyWork;

    @MetaProperty
    @Column(name = "FK_GROUP")
    protected Integer fkGroup;

    @MetaProperty
    @Column(name = "ID_PERSON")
    protected Long personId;

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getPk() {
        return pk;
    }

    public void setPk(Long pk) {
        this.pk = pk;
    }

    public void setSgNum(Integer sgNum) {
        this.sgNum = sgNum;
    }

    public Integer getSgNum() {
        return sgNum;
    }

    public void setHoursFor(Integer hoursFor) {
        this.hoursFor = hoursFor;
    }

    public Integer getHoursFor() {
        return hoursFor;
    }

    public void setWeeksCountL(Integer weeksCountL) {
        this.weeksCountL = weeksCountL;
    }

    public Integer getWeeksCountL() {
        return weeksCountL;
    }

    public void setStudentsCountL(Integer studentsCountL) {
        this.studentsCountL = studentsCountL;
    }

    public Integer getStudentsCountL() {
        return studentsCountL;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHoursCurrent(Integer hoursCurrent) {
        this.hoursCurrent = hoursCurrent;
    }

    public Integer getHoursCurrent() {
        return hoursCurrent;
    }

    public void setLecturerPercents(Integer lecturerPercents) {
        this.lecturerPercents = lecturerPercents;
    }

    public Integer getLecturerPercents() {
        return lecturerPercents;
    }

    public void setLecturerHours(Integer lecturerHours) {
        this.lecturerHours = lecturerHours;
    }

    public Integer getLecturerHours() {
        return lecturerHours;
    }


    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getWork() {
        return work;
    }

    public void setWeeksCount(Integer weeksCount) {
        this.weeksCount = weeksCount;
    }

    public Integer getWeeksCount() {
        return weeksCount;
    }

    public void setStudentsCount(Integer studentsCount) {
        this.studentsCount = studentsCount;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }

    public Integer getFkPersonLine() {
        return fkPersonLine;
    }

    public void setFkPersonLine(Integer fkPersonLine) {
        this.fkPersonLine = fkPersonLine;
    }

    public Integer getFkTrWorkChair() {
        return fkTrWorkChair;
    }

    public void setFkTrWorkChair(Integer fkTrWorkChair) {
        this.fkTrWorkChair = fkTrWorkChair;
    }

    public Integer getFkTypeStudyWork() {
        return fkTypeStudyWork;
    }

    public void setFkTypeStudyWork(Integer fkTypeStudyWork) {
        this.fkTypeStudyWork = fkTypeStudyWork;
    }

    public Integer getFkGroup() {
        return fkGroup;
    }

    public void setFkGroup(Integer fkGroup) {
        this.fkGroup = fkGroup;
    }

    @Override
    public String toString() {
        return getGroup() + ": " + getWork();
    }
}