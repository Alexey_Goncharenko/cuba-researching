package com.company.rgr.entity.fromOracle.trWorkPkg;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.chile.core.annotations.MetaProperty;

import java.io.Serializable;
import java.util.Date;
import com.haulmont.cuba.core.entity.BaseLongIdEntity;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.chile.core.annotations.NamePattern;

import javax.persistence.Column;
import javax.persistence.Entity;

@SuppressWarnings("JpaDataSourceORMInspection")
@NamePattern("%s|faculty")
@MetaClass(name = "rgr$SrcLine")
@Entity
public class SrcLine extends BaseLongIdEntity implements Updatable, Creatable, Serializable {
    private static final long serialVersionUID = 2169673985913376658L;

    @MetaProperty
    protected Date updateTs;

    @MetaProperty
    @Column(name = "CSEM")
    protected String cSem;

    @MetaProperty
    @Column(name = "SEMESTER")
    protected Integer semester;

    @MetaProperty
    @Column(name = "DISC_NAME")
    protected String subject;

    @MetaProperty
    @Column(name = "STUDY_GROUPS_NAME")
    protected String groups;

    @MetaProperty
    @Column(name = "WEEKS_COUNT")
    protected Integer weeksCount;

    @MetaProperty
    @Column(name = "STREAM_NUMBER")
    protected Integer streamNumber;

    @MetaProperty
    @Column(name = "STREAM_NUMBER2")
    protected Integer streamNumber2;

    @MetaProperty
    @Column(name = "STREAM_COUNT")
    protected Integer streamsCount;

    @MetaProperty
    @Column(name = "STUDY_GROUPS_COUNT_C")
    protected Integer groupsCount;

    @MetaProperty
    @Column(name = "STUDY_SUB_GROUPS_COUNT_C")
    protected Integer subgroupsCount;

    @MetaProperty
    @Column(name = "STUDENTS_COUNT_C")
    protected Integer studentsCount;

    @MetaProperty
    @Column(name = "VSEGO")
    protected Integer totalHours;

    @MetaProperty
    @Column(name = "HOURS_DISTRIB")
    protected Integer distributedHours;

    @MetaProperty
    @Column(name = "DELTA")
    protected Integer hoursDifference;

    @MetaProperty
    @Column(name = "PERSONS")
    protected String persons;

    @MetaProperty
    @Column(name = "LECTIONS")
    protected Integer lessonsCount;

    @MetaProperty
    @Column(name = "UST_LECTIONS")
    protected Integer setupsCount;

    @MetaProperty
    @Column(name = "CONSULTS")
    protected Integer consultsCount;

    @MetaProperty
    @Column(name = "PRACTICS")
    protected Integer practicesCount;

    @MetaProperty
    @Column(name = "LABOURS")
    protected Integer laboursCount;

    @MetaProperty
    @Column(name = "RGZS")
    protected Integer rgzsCount;

    @MetaProperty
    @Column(name = "KONTR_RABS")
    protected Integer reviewWorksCount;

    @MetaProperty
    @Column(name = "KURS_RABS")
    protected Integer courseWorksCount;

    @MetaProperty
    @Column(name = "KURS_PRS")
    protected Integer courseProjectsCount;

    @MetaProperty
    @Column(name = "ADD_HOURS")
    protected Integer additionalHoursCount;

    @MetaProperty
    @Column(name = "ZACHETS")
    protected Integer pretestsCount;

    @MetaProperty
    @Column(name = "EKZ_CONSULTS")
    protected Integer examConsultsCount;

    @MetaProperty
    @Column(name = "EKZS")
    protected Integer examsCount;

    @MetaProperty
    @Column(name = "TYPE")
    protected Integer type;

    @MetaProperty
    @Column(name = "FACULTET")
    protected String faculty;

    @MetaProperty
    protected String updatedBy;

    @MetaProperty
    protected Date createTs;

    @MetaProperty
    protected String createdBy;

    @MetaProperty
    @Column(name = "PK")
    protected Long pk;

    public Long getPk() { return pk; }

    public void setPk(Long pk) { this.pk = pk; }

    public void setCSem(String cSem) {
        this.cSem = cSem;
    }

    public String getCSem() {
        return cSem;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getGroups() {
        return groups;
    }

    public void setWeeksCount(Integer weeksCount) {
        this.weeksCount = weeksCount;
    }

    public Integer getWeeksCount() {
        return weeksCount;
    }

    public void setStreamNumber(Integer streamNumber) {
        this.streamNumber = streamNumber;
    }

    public Integer getStreamNumber() {
        return streamNumber;
    }

    public void setStreamNumber2(Integer streamNumber2) {
        this.streamNumber2 = streamNumber2;
    }

    public Integer getStreamNumber2() {
        return streamNumber2;
    }

    public void setStreamsCount(Integer streamsCount) {
        this.streamsCount = streamsCount;
    }

    public Integer getStreamsCount() {
        return streamsCount;
    }

    public void setGroupsCount(Integer groupsCount) {
        this.groupsCount = groupsCount;
    }

    public Integer getGroupsCount() {
        return groupsCount;
    }

    public void setSubgroupsCount(Integer subgroupsCount) {
        this.subgroupsCount = subgroupsCount;
    }

    public Integer getSubgroupsCount() {
        return subgroupsCount;
    }

    public void setStudentsCount(Integer studentsCount) {
        this.studentsCount = studentsCount;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }

    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }

    public Integer getTotalHours() {
        return totalHours;
    }

    public void setDistributedHours(Integer distributedHours) {
        this.distributedHours = distributedHours;
    }

    public Integer getDistributedHours() {
        return distributedHours;
    }

    public void setHoursDifference(Integer hoursDifference) {
        this.hoursDifference = hoursDifference;
    }

    public Integer getHoursDifference() {
        return hoursDifference;
    }

    public void setPersons(String persons) {
        this.persons = persons;
    }

    public String getPersons() {
        return persons;
    }

    public void setLessonsCount(Integer lessonsCount) {
        this.lessonsCount = lessonsCount;
    }

    public Integer getLessonsCount() {
        return lessonsCount;
    }

    public void setSetupsCount(Integer setupsCount) {
        this.setupsCount = setupsCount;
    }

    public Integer getSetupsCount() {
        return setupsCount;
    }

    public void setConsultsCount(Integer consultsCount) {
        this.consultsCount = consultsCount;
    }

    public Integer getConsultsCount() {
        return consultsCount;
    }

    public void setPracticesCount(Integer practicesCount) {
        this.practicesCount = practicesCount;
    }

    public Integer getPracticesCount() {
        return practicesCount;
    }

    public void setLaboursCount(Integer laboursCount) {
        this.laboursCount = laboursCount;
    }

    public Integer getLaboursCount() {
        return laboursCount;
    }

    public void setRgzsCount(Integer rgzsCount) {
        this.rgzsCount = rgzsCount;
    }

    public Integer getRgzsCount() {
        return rgzsCount;
    }

    public void setReviewWorksCount(Integer reviewWorksCount) {
        this.reviewWorksCount = reviewWorksCount;
    }

    public Integer getReviewWorksCount() {
        return reviewWorksCount;
    }

    public void setCourseWorksCount(Integer courseWorksCount) {
        this.courseWorksCount = courseWorksCount;
    }

    public Integer getCourseWorksCount() {
        return courseWorksCount;
    }

    public void setCourseProjectsCount(Integer courseProjectsCount) {
        this.courseProjectsCount = courseProjectsCount;
    }

    public Integer getCourseProjectsCount() {
        return courseProjectsCount;
    }

    public void setAdditionalHoursCount(Integer additionalHoursCount) {
        this.additionalHoursCount = additionalHoursCount;
    }

    public Integer getAdditionalHoursCount() {
        return additionalHoursCount;
    }

    public void setPretestsCount(Integer pretestsCount) {
        this.pretestsCount = pretestsCount;
    }

    public Integer getPretestsCount() {
        return pretestsCount;
    }

    public void setExamConsultsCount(Integer examConsultsCount) {
        this.examConsultsCount = examConsultsCount;
    }

    public Integer getExamConsultsCount() {
        return examConsultsCount;
    }

    public void setExamsCount(Integer examsCount) {
        this.examsCount = examsCount;
    }

    public Integer getExamsCount() {
        return examsCount;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }


    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }


    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public String toString() {
        return getId() + ": " + getSubject();
    }
}